# 基于TensorRT实现的YOLOX

## 依赖

- CUDA
- TensorRT
- OpenCV

## 编译

首先在`CMakeLists.txt`文件中设置`CUDA`和`TensorRT`的安装路径：

```
set(CUDA_PATH /usr/local/cuda)
set(TENSORRT_PATH /path/to/TensorRT)
```

然后执行下面的命令进行编译：

```shell
mkdir build
cd build
cmake ..
make
```

编译成功后会生成可执行文件`tensorrt_yolox`。

## 执行

执行下面的命令启动程序:

```shell
./tensorrt_yolox </path/to/onnx/model> </path/to/video>
```

比如：

```shell
./tensorrt_yolox ../models/yolox_s.onnx ../video/test.mp4
```

程序默认会把检测结果保存到`result.mp4`文件中。

**扫描下面二维码关注我的公众号【DeepDriving】，我会持续分享计算机视觉、机器学习、深度学习、无人驾驶等领域的文章**
![](https://tuchuang-1312078823.cos.ap-guangzhou.myqcloud.com/markdown/gongzhonghao.jpg)
